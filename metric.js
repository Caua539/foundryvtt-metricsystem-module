import { DND5E } from "../../systems/dnd5e/module/config.js";

class MetricModule {
    static onRenderActorSheet(obj, html, data) {
        const users = game.users.entities;
        const user = users.find(u => u.data._id === game.userId);
        if (user.data.role >= 3 && !game.settings.get("metric-system-dnd5e", "disableButton")) {
            let element = html.find(".window-header .window-title")
            MetricModule.addButton(element, game.actors.entities.find(a => a.data._id === obj.object.data._id));
        }
    }
    
    static addButton(element, actor) {
        // Can't find it?
        if (element.length != 1) {
            return;
        }
        let button = $(`<a class="popout" style><i class="fas fa-ruler"></i>${game.i18n.localize("metricsystem.button.name")}</a>`);
        button.on('click', (event) => chooseTool(actor));
        element.after(button);
    }

}

function getLocalizedItemType(type) {

    let htmlstart = '<p class="metric-name"><span style="color:';
    let htmlend = ': </span>'

    if (type === "weapon") {
        return htmlstart+'darkred;">'+game.i18n.localize("metricsystem.converter.type.weapon")+htmlend;
    } else if (type === "spell") {
        return htmlstart+'darkmagenta;">'+game.i18n.localize("metricsystem.converter.type.spell")+htmlend;
    } else if (type === "tool") {
        return htmlstart+'dimgrey;">'+game.i18n.localize("metricsystem.converter.type.tool")+htmlend;
    } else if (type === "feat") {
        return htmlstart+'limegreen;">'+game.i18n.localize("metricsystem.converter.type.feat")+htmlend;
    } else if (type === "consumable") {
        return htmlstart+'orangered;">'+game.i18n.localize("metricsystem.converter.type.consume")+htmlend;
    } else if (type === "equipment") {
        return htmlstart+'steelblue;">'+game.i18n.localize("metricsystem.converter.type.equip")+htmlend;
    } else if (type === "backpack") {
        return htmlstart+'saddlebrown;">'+game.i18n.localize("metricsystem.converter.type.backpack")+htmlend;
    } else if (type === "loot") {
        return htmlstart+'midnightblue;">'+game.i18n.localize("metricsystem.converter.type.loot")+htmlend;
    }
}

function setRangeDisplay(range, long=false) {
    if (!long) {
        if (range.value) {
            return range.value+' <span style="color:red">'+range.units+"</span>";
        } else {
            return " - ";
        }
    } else {
        if (range.long) {
            return range.long+' <span style="color:red">'+range.units+"</span>";
        } else {
            return " - ";
        }
    }
}

function setTargetDisplay(target) {
    if (target.value && ["ft", "mi", "m", "km"].includes(target.units)) {
        return target.value+' <span style="color:red">'+target.units+"</span> "+DND5E.targetTypes[target.type];
    } else {
        return " - ";
    }
}

async function chooseTool(actor) {
    if (game.settings.get("metric-system-dnd5e", "enableWeight")) {
        let template = "./modules/metric-system-dnd5e/template/dialog.html";
        let dialogData = {};
        const html = await renderTemplate(template, dialogData);
        let d = new Dialog({
            title: game.i18n.localize("metricsystem.window.name"),
            content: html,
            buttons: {
            one: {
                icon: '<i class="fas fa-ruler"></i>',
                label: game.i18n.localize("metricsystem.window.distance.name"),
                callback: html => new DistanceForm(actor).render(true)
            },
            two: {
                icon: '<i class="fas fa-weight-hanging"></i>',
                label: game.i18n.localize("metricsystem.window.weight.name"),
                callback: html => new WeightForm(actor).render(true)
            }
            },
            close: html => console.log()
        });

        d.render(true);
    } else {
        new DistanceForm(actor).render(true);
    }
}

class DistanceForm extends FormApplication {
    constructor(actor){
        super(actor);
        this.actor = actor;
    }
    
    /**
     * Default Options for this FormApplication
     */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: "distance-form",
            title: game.i18n.localize("metricsystem.window.distance.name"),
            template: "./modules/metric-system-dnd5e/template/distance-form.html",
            classes: ["sheet"],
            width: 600,
            closeOnSubmit: true
        });
    }

    /**
     * Provide data to the handlebars template
     */
    async getData() {
        let items = this.actor.data.items;
        let valid = []
        items.forEach(function(item) {
            if (item.data.range || item.data.target) {
                let range = item.data.range;
                let target = item.data.target;
                if ((range.value || range.long) && (["ft", "mi", "m", "km"].includes(range.units))) {
                    valid.push(item)
                } else if (target.value && (["ft", "mi", "m", "km"].includes(target.units))) {
                    valid.push(item)
                }
            }
        })

        this.list = valid.map(t => ({
            name: getLocalizedItemType(t.type)+t.name+"</p>",
            id: t._id,
            range: setRangeDisplay(t.data.range),
            long: setRangeDisplay(t.data.range, true),
            target: setTargetDisplay(t.data.target),
        }))
        this.list.sort(function(a,b){
            return a.name.localeCompare(b.name);
        })
        return {
            ...super.getData(),
            rows: this.list
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        
        html.find('[id="cb-select-all"]').click(() => {
            if(this.checked) {
                this.checked = false;
                // Iterate each checkbox
                $('.metric-1').each(function() {
                    this.checked = false;                    
                });
            } else {
                this.checked = true;
                $('.metric-1').each(function() {
                    this.checked = true;
                });
            }
        });
    }

    async _updateObject(event, formData) {
        let updateItems = []
        let system = $("input[name='convert']:checked").val();
        let option = $("input[name='onlyunits']:checked").val();

        let formarray = Object.keys(formData);
        formarray.forEach(iid => {
            if (iid === "convert") {
                return;
            } else if (iid === "onlyunits") {
                return;                
            }
        
            if (formData[iid]) {
                let item = this.actor.getOwnedItem(iid);
                let data = item.data.data;

                let updateData = {
                    _id: item._id,
                    data: {}
                }

                let rangeunit;
                let targetunit;
                let rangevalue;
                let rangelong;
                let targetvalue;

                if (["ft", "m"].includes(data.range.units)) {
                    if (data.range.value) {
                        if (!option) {rangevalue = system === "m" ? data.range.value * 0.3 : Math.trunc(data.range.value / 0.3);}
                        rangeunit = system === "m" ? "m" : "ft";
                    }
                    if (data.range.long) {
                        if (!option) {rangelong = system === "m" ? data.range.long * 0.3 : Math.trunc(data.range.long / 0.3);}
                        rangeunit = system === "m" ? "m" : "ft";
                    }
                } else if (["mi", "km"].includes(data.range.units)) {
                    if (data.range.value) {
                        if (!option) {rangevalue = system === "m" ? data.range.value * 1.5 : Math.trunc(data.range.value / 1.5);}
                        rangeunit = system === "m" ? "km" : "mi";
                    }
                    if (data.range.long) {
                        if (!option) {rangelong = system === "m" ? data.range.long * 1.5 : Math.trunc(data.range.long / 1.5);}
                        rangeunit = system === "m" ? "km" : "mi";
                    }
                }
                if (["ft", "m"].includes(data.target.units)) {
                    if (data.target.value) {
                        if (!option) {targetvalue = system === "m" ? data.target.value * 0.3 : Math.trunc(data.target.value / 0.3);}
                        targetunit = system === "m" ? "m" : "ft";
                    }
                } else if (["mi", "km"].includes(data.target.units)) {
                    if (data.target.value) {
                        if (!option) {targetvalue = system === "m" ? data.target.value * 1.5 : Math.trunc(data.target.value / 1.5);}
                        targetunit = system === "m" ? "km" : "mi";
                    }
                }

                if (rangevalue || rangeunit) {
                    updateData.data = updateData.data || {};
                    updateData.data.range = updateData.data.range || {};
                    if (!option) updateData.data.range.value = rangevalue;
                    updateData.data.range.units = rangeunit;
                }
                if (rangelong || rangeunit) {
                    updateData.data = updateData.data || {};
                    updateData.data.range = updateData.data.range || {};
                    if (!option) updateData.data.range.long = rangelong;
                    updateData.data.range.units = rangeunit;
                }
                if (targetvalue || targetunit) {
                    updateData.data = updateData.data || {};
                    updateData.data.target = updateData.data.target || {};
                    if (!option) updateData.data.target.value = targetvalue;
                    updateData.data.target.units = targetunit;
                }

                updateItems.push(updateData)
            }
        })

        await this.actor.updateEmbeddedEntity("OwnedItem", updateItems);
        ui.notifications.info(game.i18n.localize("metricsystem.converter.notification"));
        console.log("%cMetric System for DnD5e"+" | Selected OwnedItems' range and/or target attributes converted successfully.", "color:green");
    }
}

class WeightForm extends FormApplication {
    constructor(actor){
        super(actor);
        this.actor = actor;
    }
    
    /**
     * Default Options for this FormApplication
     */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: "weight-form",
            title: game.i18n.localize("metricsystem.window.weight.name"),
            template: "./modules/metric-system-dnd5e/template/weight-form.html",
            classes: ["sheet"],
            width: 500,
            closeOnSubmit: true
        });
    }

    /**
     * Provide data to the handlebars template
     */
    async getData() {
        let items = this.actor.data.items;
        let valid = []
        items.forEach(function(item) {
            if (item.data.weight) {
                if (item.data.weight) {
                    valid.push(item)
                }
            }
        })

        this.list = valid.map(t => ({
            name: getLocalizedItemType(t.type)+t.name+"</p>",
            id: t._id,
            weight: t.data.weight ? t.data.weight : " - ",
        }))

        this.list.sort(function(a,b){
            return a.name.localeCompare(b.name);
        })
        return {
            ...super.getData(),
            rows: this.list
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        
        html.find('[id="cb-select-all"]').click(() => {
            if(this.checked) {
                this.checked = false;
                // Iterate each checkbox
                $('.metric-1').each(function() {
                    this.checked = false;                    
                });
            } else {
                this.checked = true;
                $('.metric-1').each(function() {
                    this.checked = true;
                });
            }
        });
    }

    async _updateObject(event, formData) {
        let updateItems = []
        let system = $("input[name='convert']:checked").val();

        let formarray = Object.keys(formData);
        formarray.forEach(iid => {
            if (iid === "convert") {
                return;
            }
        
            if (formData[iid]) {
                console.log(this.actor.getOwnedItem(iid))
                let item = this.actor.getOwnedItem(iid);
                let data = item.data.data;

                let weightvalue;

                if (data.weight) {
                    weightvalue = system === "kg" ? data.weight * 0.5 : data.weight * 2;
                }

                let updateData = {
                    _id: item._id,
                    data: {}
                }

                if (weightvalue) {
                    updateData.data = updateData.data || {};
                    updateData.data.weight = weightvalue
                }
                //item.prepareData()
                updateItems.push(updateData)
            }
        })

        await this.actor.updateEmbeddedEntity("OwnedItem", updateItems);
        ui.notifications.info(game.i18n.localize("metricsystem.converter.notification"));
        console.log("%cMetric System for DnD5e"+" | Selected OwnedItems' weight attribute converted successfully.", "color:green");
        //console.log(actor)
    }
}

Hooks.once('init', () => {
    game.settings.register("metric-system-dnd5e", "disableButton", {
		name: game.i18n.localize("metricsystem.settings.disableBttn.name"),
		hint: game.i18n.localize("metricsystem.settings.disableBttn.hint"),
		scope: "world",
		config: true,
		default: false,
		type: Boolean
    });
    
    game.settings.register("metric-system-dnd5e", "enableWeight", {
		name: game.i18n.localize("metricsystem.settings.enableWeight.name"),
		hint: game.i18n.localize("metricsystem.settings.enableWeight.hint"),
		scope: "world",
		config: true,
		default: false,
        type: Boolean,
        onChange: x => {
            window.location.reload();
        }
    });
});

Hooks.on('init', () => {
    // Unit Labels - Add "meters" and "kilometers" as movement and distance options, putting them first on the list.
    console.log("%cMetric System for DnD5e"+" | Adding labels 'Meters' and 'Kilometers' to Movement and Distance unit options.", "color:green")
    DND5E.movementUnits = {}
	DND5E.movementUnits["m"] = game.i18n.localize("metricsystem.meters");
    DND5E.movementUnits["km"] = game.i18n.localize("metricsystem.kilometers");
    DND5E.movementUnits["ft"] = game.i18n.localize("DND5E.DistFt");
    DND5E.movementUnits["mi"] = game.i18n.localize("DND5E.DistMi");

    delete DND5E.distanceUnits["ft"];
    delete DND5E.distanceUnits["mi"];

    for ( let [k, v] of Object.entries(DND5E.movementUnits) ) {
        DND5E.distanceUnits[k] = v;
    }

    // Encumbrance Calculation - Change emcumbrance values and multipliers to Metric
    if (game.settings.get("metric-system-dnd5e", "enableWeight")) {
        console.log("%cMetric System for DnD5e"+" | Changing encumbrance calculation to Metric.", "color:green")
        DND5E.encumbrance["currencyPerWeight"] = 100;
        DND5E.encumbrance["strMultiplier"] = 7.5;
        DND5E.encumbrance["vehicleWeightMultiplier"] = 1000;
    }
});

Hooks.on('ready', () => {
    if (game.settings.get("metric-system-dnd5e", "enableWeight")) {
        console.log("%cMetric System for DnD5e"+" | Changing label 'lbs.' to 'kg'.", "color:green")
        game.i18n.translations.DND5E["AbbreviationLbs"] = 'kg';
    }
    console.log("%cMetric System for DnD5e"+" | Changing label 'ft.' to 'm.'.", "color:green")
    game.i18n.translations.DND5E["FeetAbbr"] = 'm.';
    
});

Hooks.on('renderActorSheet', MetricModule.onRenderActorSheet);

Hooks.on('preCreateScene', (scenedata) => {
    // Scene Creation - Change Scene Measurements to Metric 
    console.log("%cMetric System for DnD5e"+" | New Scene: changing gridUnits to 'm' and gridDistance to '1.5'.", "color:green");
    scenedata.gridDistance = 1.5;
    scenedata.gridUnits = "m";
})

Hooks.on('preCreateActor', (actor) => {
    if (actor.data == null) {
        // Actor Creation - Change Actor Base Speed to Metric
        console.log("%cMetric System for DnD5e"+" | NEW Actor: changing movementUnits to 'm' and walk speed to '9'.", "color:green");
        actor["data"] = {
            attributes: {
                movement: {
                    walk: 9,
                    units: "m"
                }
            }
        };
        
        // Token Prototype Creation - Change Token Base Dim Vision to Metric
        console.log("%cMetric System for DnD5e"+" | NEW Actor Token: changing dimSight to 9 units of distance.", "color:green")
        actor.token.dimSight = 9
    } else {
        console.log("%cMetric System for DnD5e"+" | 'New' Actor: '"+actor.name+"' is being imported or duplicated. In these cases, we can't determine if its movement and sight were already in metric without making assumptions, so nothing is being changed. Please edit the actor manually.", "color:green")
    }

});